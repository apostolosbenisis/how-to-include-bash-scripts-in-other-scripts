# README #

Assume you have a script `main.sh` located in `~/scripts/` and you would like to include the script `foo.sh`, which is also located in the same folder.

Here is the folder structure

```
~
+-- scripts
   +-- main.sh
   +-- foo.sh
```

In `main.sh`:
```
#!/bin/bash
source 'foo.sh'
```

In `foo.sh`:
```
#!/bin/bash
echo "foo"
```

When the current working directory is set to where `main.sh` is located, the code will work:
```
~$ cd scripts
~/scripts$ bash main.sh
foo
```

However, when the current working directory is some other arbitary location, it will not work:
```
~$ cd /
/$ bash ~/scripts/main.sh
line 2: foo.sh: No such file or directory
```

The reason is that `source 'foo.sh'` will try to interpret the path to the script as beeing relative to the current working directory.

To solve this problem `foo.sh` must be sourced either as absolute path `source '~/scripts/foo.sh'`. The problem in this case is that if the scripts are moved to another location, the code will break.

A better solution is to to source the script as relative to the location of `main.sh` and dynamically [get the source directory of `main.sh` from within the script itself](https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself).

In `main.sh`:

```
#!/bin/bash

MAIN_SCRIPT_ABS_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${MAIN_SCRIPT_ABS_PATH}/foo.sh"
```

Now it is possible to execut `main.sh` from an arbitary location:
```
~$ cd /
/$ bash ~/scripts/main.sh
foo
```


