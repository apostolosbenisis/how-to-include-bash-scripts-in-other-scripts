#!/usr/bin/env bash

# will only work when executing this script from the source location
source 'foo.sh'

# will work even when this script is eecute from an arbitary location
THIS_SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${THIS_SCRIPT_PATH}/foo.sh"
